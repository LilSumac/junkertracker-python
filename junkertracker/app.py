class JTApp:
    _debug_mode = False
    _last_cmd = None

    def __init__(self, debug_mode=False):
        self._debug_mode = debug_mode

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    @staticmethod
    def print_debug(*args, sep='', end='\n', file=None, flush=False):
        print("[DEBUG] ", end='')
        print(*args, sep=sep, end=end, file=file, flush=flush)

    def should_run(self):
        # TODO: More quit cases?
        return self._last_cmd != "q"

    def poll_cmd(self):
        input_txt = input(self.get_input_prefix())

        # TODO: Dispatch command to handler.
        self.print_debug("You input ", input_txt)
        self._last_cmd = input_txt

    def get_input_prefix(self):
        return "INPUT>"
