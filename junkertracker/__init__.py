# Main file
from junkertracker.app import JTApp


def main():
    with JTApp() as main_app:
        # Initialize?

        while main_app.should_run():
            main_app.poll_cmd()

        # Clean up?


if __name__ == "__main__":
    main()
